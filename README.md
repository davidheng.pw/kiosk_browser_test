# kiosk_browser_test



## Main goal

The main goal of this project is to bring a lightweight browser for embbeded system to be used in kiosk mode to watch some webpage like dashboard and more. Also this project target Buildroot for adding some packages to bring the browser.

## Testing browser

For now some browsers were tested on Rasperry OS to make a proof of concept and de possibility of this project.

Chromium, Midori were tested and you can find the bash script in this repo. However, without CLANG in last Buildroot packages, Chromium integration will be very hard and for Midori even if it has his own package in Buildroot, the Midori project is actually dead and do not work well for now.

The actual minimal browser which is tested now is Qutebrowser. Qutebrowser is based onn Python and PyQt5 (Qt5) package which are available on Buildroot. You can find the bash script in the repo.


