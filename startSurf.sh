#!/bin/bash

#parsing des URLs
url=$(cat url.txt)

xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

surf -F $url
