#!/bin/bash

#URLs parsing
url=$(cat url.txt)

xset s noblank #don't blank the video device 
xset s off #disable screen saver blanking
xset -dpms #disable DPMS (Energy Star) features

unclutter -idle 0.5 -root & #hiding mouse cursor

matchbox-window-manager & #enable window manager to show browser interface with full screen

qutebrowser -R $url & #running Qutebrowser by opening URLs from url.txt file.

#Every 5 s, enabled hotkeys will switch the tabs of Qutebrowser.
while true; do
   sleep 5
   xdotool keydown ctrl+Page_Down; xdotool keyup Page_Down;
done
