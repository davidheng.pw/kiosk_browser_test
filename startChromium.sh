#!/bin/bash

#parsing des URLs
url=$(cat url.txt)

xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

chromium-browser $url  --start-fullscreen  --window-size=1920,1080  --disable-translate  --no-sandbox --noerrdialogs --disable-infobars --kiosk &

while true; do
   sleep 25
   xdotool keydown ctrl+Tab; xdotool keyup ctrl+Tab;
done
