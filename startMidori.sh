#!/bin/bash

#parsing des URLs
url=$(cat url.txt)

xset -dpms
xset s off
xset s noblank
unclutter &
matchbox-window-manager &
midori -e Fullscreen $url
while true; do
     sleep 5
     xdotool keydown ctrl+Tab; xdotool keyup ctrl+Tab;
done
